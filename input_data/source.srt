1
00:00:00,000 --> 00:00:11,186
Subtitles were automatically generated using IITM Speech Lab (Umesh) ASR system version-10092020

2
00:00:16,082 --> 00:00:19,729
The last part of LED that remains is emission pattern.

3
00:00:21,536 --> 00:00:23,263
Let us say I have a LED.

4
00:00:24,570 --> 00:00:30,342
and I am looking at, when we say, emission pattern we typically look at far field pattern.

5
00:00:33,281 --> 00:00:43,760
far field patterns are those when we look at distances much larger than λ.

6
00:00:45,347 --> 00:00:49,200
So, we are talking about much larger distances when compared to λ.

7
00:00:51,080 --> 00:00:53,494
Why are we interested in looking at the emission pattern?

8
00:00:54,227 --> 00:00:57,240
As far as the communication grade LEDs are concerned.

9
00:00:57,307 --> 00:01:00,440
we have to couple that light into an optical fiber.

10
00:01:00,587 --> 00:01:01,854
So you need to know

11
00:01:02,080 --> 00:01:11,546
What is the numerical aperture? What is the emission width of your LED and is that within the numerical aperture of the fiber.

12
00:01:11,612 --> 00:01:14,626
So can I have an emission pattern like this.

13
00:01:14,986 --> 00:01:20,399
When we talk about fibre, optical fiber will always have a certain acceptance angle.

14
00:01:20,999 --> 00:01:29,305
So if your acceptance angle is within the emission width, then this light can be coupled into the fiber.

15
00:01:29,652 --> 00:01:37,318
if this acceptance cone is very small, but LED is emitting really wide angle, it means a lot of light you are losing.

16
00:01:38,745 --> 00:01:42,345
Only a part of the light is getting coupled into the fiber.

17
00:01:42,931 --> 00:01:58,643
Ideally if you were to couple lights into a fiber the numerical aperture of the angular spread of the LED should match with the acceptance angle of your fiber, so that you are able to optimally couple light into the fiber

18
00:01:59,563 --> 00:02:11,763
But as far as display series are concerned, right, not for communication, for display LEDs are concerned, you would want this emission to be as broad as possible. So, that light is seen from all possible angles.

19
00:02:13,203 --> 00:02:27,004
Now the question for you to think about is, we already said that, you know from inside the LED structure, there is only a certain, so lets say this is the emitting structure.

20
00:02:29,124 --> 00:02:34,045
light that is coming at angles greater than the critical angle are going to be internally reflected.

21
00:02:34,857 --> 00:02:43,497
which means only a certain cone of light is emitted and that cone is decided by the critical angle.

22
00:02:44,999 --> 00:02:45,492
ok.

23
00:02:46,657 --> 00:02:47,612
So

24
00:02:49,106 --> 00:03:00,172
Then why are we talking about this, you know, emission pattern. Can I not just say directly that the emission pattern is directly defined by the critical angle.

25
00:03:03,437 --> 00:03:07,290
can I just not do sin inverse 1 over N and say that that is my emission angle.

26
00:03:07,315 --> 00:03:09,421
Of course I have to allow for the refraction also

27
00:03:09,861 --> 00:03:10,808
So there is a

28
00:03:11,088 --> 00:03:23,032
I will have a certain angle of incidence within which if it is arriving, this is the arrival rate, angle of arrival at the interface.

29
00:03:23,302 --> 00:03:31,125
if the angle of arrival is within the critical angle, you will have refraction and you will have some light that is coming out.

30
00:03:31,150 --> 00:03:37,396
but if I have a ray that is arriving at an angle greater than the critical angle that is getting lost in the system

31
00:03:38,409 --> 00:03:44,246
So, can I not just say directly that the twice the critical angle.

32
00:03:45,006 --> 00:03:50,086
And ofcourse it is not directly twice a critical angle you have to find out the corresponding angle of refraction corresponding to that.

33
00:03:50,366 --> 00:03:53,606
Can that not be directly considered as the emission angle of the LED

34
00:03:55,795 --> 00:03:58,782
In fact, you say that the LED emission is a Lamberitan.

35
00:04:00,272 --> 00:04:01,405
What is the Lamberitan?

36
00:04:01,685 --> 00:04:05,405
Intensity falling exponentially is not a Lamberitan.

37
00:04:05,738 --> 00:04:18,565
Lamberitan is an emission where, lets say this is the emitting surface and this is the normal to the surface and you are looking at an angle θ with respect to the normal.

38
00:04:18,590 --> 00:04:23,644
So lets say I have an emitting surface like this, this is my θ, this is my normal.

39
00:04:23,911 --> 00:04:28,003
and i am looking at an angle θ anywhere around this normal

40
00:04:28,616 --> 00:04:30,034
and I am making a plot.

41
00:04:30,974 --> 00:04:40,589
ok, If I am making a plot of the emission. So emission is maximum obviously at θ equal to 0.

42
00:04:41,629 --> 00:04:44,895
because the refraction is minimum there.

43
00:04:44,962 --> 00:04:47,047
the final reflection is also minimum there.

44
00:04:48,109 --> 00:04:53,038
If this I call as I0, the intensity at maximum.

45
00:04:53,494 --> 00:05:00,200
And if I at any angle is equal to I0 cos θ.

46
00:05:02,092 --> 00:05:09,358
Then it is a Lamberitan source. A Lamberitan source is a one where I th θ is I0 cos θ

47
00:05:11,305 --> 00:05:12,878
So if I do a polar diagram

48
00:05:14,385 --> 00:05:20,038
I mark angles corresponding to forty five degree sixty degree etc

49
00:05:20,585 --> 00:05:28,003
So, this is let us say 30 degree, this is 45 degree, this is 60 degree and θ is measured with respect to the normal.

50
00:05:28,980 --> 00:05:31,887
How would the polar plot of a Lamberitan look like?

51
00:05:34,555 --> 00:05:38,035
peak is I0 and it goes as cos θ.

52
00:05:38,815 --> 00:05:42,241
So which means at θ equal to 0, it is maximum.

53
00:05:42,668 --> 00:05:47,641
θ equal to 90 degree it is minimum which is 0.

54
00:05:48,003 --> 00:05:58,536
θ equal to 45 degree, I0 by root 2. when does it become half of I0? 60 degree and so on.

55
00:05:58,817 --> 00:05:59,950
So if I plot this

56
00:06:00,242 --> 00:06:01,441
This is going to look like a

57
00:06:10,456 --> 00:06:13,882
This is not come out really to scale nicely. But

58
00:06:16,195 --> 00:06:20,128
at θ equal to 60 degree, this is θ equal to 60 degree.

59
00:06:20,472 --> 00:06:24,351
this should become I0 divided by 2.

60
00:06:25,207 --> 00:06:29,461
and typically LED emission pattern is a Lamberitan.

61
00:06:35,382 --> 00:06:40,369
Now, how does this tie up with the fact that the emission is restricted by total internal reflection.

62
00:06:41,982 --> 00:06:45,435
On one hand we are saying it is restricted by total internal reflection.

63
00:06:45,688 --> 00:06:49,795
On the other hand, we say that surface emitting LEDs are all Lamberitan.

64
00:06:52,914 --> 00:06:55,234
So how are these two matching?

65
00:06:56,061 --> 00:07:00,280
There is something else also happens before you make a commercial LED.

66
00:07:00,354 --> 00:07:11,539
So, you have the junction emitting, so if this is your junction and let us say you have emission. So the emission from the junction is of course limited by total internal reflection.

67
00:07:12,153 --> 00:07:19,193
But, what you also do is that you put a dome, which is like an epoxy dome.

68
00:07:21,005 --> 00:07:33,378
So, that even within the dome it is restricted to a certain angle, what comes outside the dome is further refracted, so that the angular spread is increased.

69
00:07:34,578 --> 00:07:38,404
So, people use any LED, you see that dome on top of the LED.

70
00:07:38,897 --> 00:07:43,042
What is the dome used for? The dome is used to actually spread the light out

71
00:07:45,008 --> 00:07:48,755
So, that you are not confined within that total internal reflection angle.

72
00:07:49,808 --> 00:07:54,037
And done is only one simple structure to manipulate the emission angle.

73
00:07:54,448 --> 00:08:04,281
You could have lenses inside the LED, you could have reflectors inside the LED.

74
00:08:05,254 --> 00:08:06,668
The other day, I had a question.

75
00:08:06,987 --> 00:08:10,614
What happens to the light that is getting reflected back into the system.

76
00:08:11,840 --> 00:08:13,214
right. Ah

77
00:08:13,667 --> 00:08:21,093
If the angle of incidence is greater than the total internal reflection angle, a critical angle, you have reflection is that completely getting trapped?

78
00:08:21,840 --> 00:08:28,772
So, it is actually trapped inside the system, but you can keep reflectors inside the system so that the trap light can also come out of the system.

79
00:08:29,466 --> 00:08:37,267
Of course, these days people are engineering structures inside the LED so that the light extraction capacity from the LED is larger and larger.

80
00:08:37,853 --> 00:08:55,514
Because the other day when we worked out the biggest hit on efficiency, we are taking from not because of the internal quantum efficiency, but because of there external quantum efficiency, ability of your structure to extract the lights out. So people are still working out.

81
00:08:56,314 --> 00:09:07,698
What is the best way to people have put in, what are called as photonic band gap structures so that specific directions are extracted out of the system and so on.

82
00:09:08,778 --> 00:09:18,430
ok. so by putting dome lenses reflectors etc, you can manipulate the emission angle.

83
00:09:18,764 --> 00:09:30,514
but as far as the LEDs for optical communication is concerned, what we are interested is not to increase the emission angle arbitrarily large.

84
00:09:30,753 --> 00:09:48,216
We want to match the angle of divergence of LED with the numerical aperture of the fiber.

85
00:09:53,079 --> 00:10:05,306
We will talk about what a numerical aperture of a fiber, when we talk about fibers. But from your elementary class 12 understanding you know that every optical fiber depending on the refractive index of the core.

86
00:10:06,661 --> 00:10:12,341
and the refractive index of the cladding can accept only light at specific angles.

87
00:10:13,008 --> 00:10:17,794
and that is your acceptance angle and sine of, so this is your θ c

88
00:10:21,107 --> 00:10:26,856
and sin of that acceptance angle is your numerical aperture. numerical aperture is sin θ c.

89
00:10:27,223 --> 00:10:36,028
So as far as LEDs for optical communication are concerned you are trying to match the emission angle of the LED with that of the numerical aperture of the fiber.

90
00:10:39,351 --> 00:10:49,282
Typically display LEDs are surface emitting LEDs so that you can have a large acceptance angle whereas communication LEDs are edge emitting LEDs.

91
00:10:55,122 --> 00:11:07,068
If you remember the diagram of the PN junction, the edge emit, if this is the active region, the junction region edge emitting LEDs emit from the side whereas the surface emitting LEDs emit from the surface

92
00:11:07,108 --> 00:11:17,596
Right, we would like to have edge emitting LEDs so that you are ensuring some amount of guidance inside the double hetero structure and you can control this θ.

93
00:11:20,773 --> 00:11:24,525
So, that completes our discussion on LEDs.
